package br.edu.up.listview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    final String[] nomes = {"João", "Maria", "Pedro", "João", "Maria", "Pedro", "João", "Maria", "Pedro"};

    MusicaAdapter adapter = new MusicaAdapter(this, nomes);
    //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, nomes);

    ListView lv = (ListView) findViewById(R.id.listView);
    lv.setAdapter(adapter);
    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Toast.makeText(MainActivity.this, "Nome: " + nomes[position],Toast.LENGTH_LONG).show();
      }
    });

  }
}
