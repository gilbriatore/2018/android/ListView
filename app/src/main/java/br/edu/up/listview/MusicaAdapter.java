package br.edu.up.listview;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by GIL on 13/09/2016.
 */
public class MusicaAdapter extends BaseAdapter {

  private Context ctx;
  private String[] nomes;

  public MusicaAdapter(Context ctx, String[] nomes){
    this.ctx = ctx;
    this.nomes = nomes;
  }


  @Override
  public int getCount() {
    return nomes.length;
  }

  @Override
  public Object getItem(int position) {
    return nomes[position];
  }

  @Override
  public long getItemId(int id) {
    return id;
  }

  @Override
  public View getView(int position, View view, ViewGroup viewGroup) {

    TextView tv = new TextView(ctx);
    tv.setTextSize(20);
    tv.setText(nomes[position]);

    return tv;
  }
}
